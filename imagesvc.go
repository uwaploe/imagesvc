// Package imagesvc defines the gRPC interface to the inVADER Image
// acquisition server.
package imagesvc

import (
	context "context"
	"time"

	grpc "google.golang.org/grpc"
)

// The Protobuf definition is stored in the server's repository. Use the
// Buf tool to generate the Go stub (see https://buf.build/docs/introduction)

//go:generate buf image build --source https://bitbucket.org/uwaploe/imagerpc.git#branch=master -o image.bin
//go:generate protoc --descriptor_set_in=image.bin --go_out=plugins=grpc:. imagesvc.proto
//go:generate rm -f image.bin

type TriggerType string

const (
	HardwareTrigger TriggerType = "HARDWARE"
	SoftwareTrigger             = "SOFTWARE"
)

// AcqOptions is an image acquisition option
type AcqOption struct {
	f func(*ImageSettings, *AcqRequest)
}

type acqOptions struct {
	s ImageSettings
}

// Exposure specifies the camera exposure time
func Exposure(val time.Duration) AcqOption {
	return AcqOption{func(opts *ImageSettings, _ *AcqRequest) {
		opts.Exposure = uint32(val / time.Microsecond)
	}}
}

// Timeout specifies the maximum allowed acquisition time
func Timeout(val time.Duration) AcqOption {
	return AcqOption{func(_ *ImageSettings, req *AcqRequest) {
		req.Timeout = uint32(val / time.Millisecond)
	}}
}

// Format sets the format for the acquired image.
func Format(val string) AcqOption {
	return AcqOption{func(opts *ImageSettings, _ *AcqRequest) {
		opts.Format = val
	}}
}

// Gain sets the camera gain in dB
func Gain(val float32) AcqOption {
	return AcqOption{func(opts *ImageSettings, _ *AcqRequest) {
		opts.Gain = val
	}}
}

// Trigger sets the cammera trigger type
func Trigger(val TriggerType) AcqOption {
	return AcqOption{func(opts *ImageSettings, _ *AcqRequest) {
		opts.Trigger = ImageSettings_TriggerSource(ImageSettings_TriggerSource_value[string(val)])
	}}
}

// Client combines the gRPC ImageAcqClient with image acquisition settings.
type Client struct {
	*imageAcqClient
	params ImageSettings
	req    AcqRequest
}

func NewClient(conn *grpc.ClientConn) *Client {
	c := Client{}
	c.imageAcqClient = &imageAcqClient{conn}
	// Set some sensible defaults
	c.params = ImageSettings{
		Format:   "XI_MONO16",
		Exposure: 10000,
		Gain:     -1.0,
		Trigger:  ImageSettings_SOFTWARE,
	}
	c.req = AcqRequest{
		Timeout: 10000,
	}
	return &c
}

// StartAcq sets the image parameters and enables acquisition, this function must be
// called before each invocation of Snapshot or Acquire. The supplied options are
// persistent from one call to the next.
func (c *Client) StartAcq(ctx context.Context, opts ...AcqOption) error {
	for _, opt := range opts {
		opt.f(&c.params, &c.req)
	}
	_, err := c.imageAcqClient.StartAcq(ctx, &c.params)
	return err
}

// Acquire acquires and streams a single image at the next trigger
func (c *Client) Acquire(ctx context.Context) (*StreamReader, error) {
	stream, err := c.imageAcqClient.Acquire(ctx, &c.req)
	if err != nil {
		return nil, err
	}
	return &StreamReader{stream: stream}, nil
}

// SendTrigger sends a software trigger to the camera.
func (c *Client) SendTrigger(ctx context.Context) error {
	_, err := c.imageAcqClient.SoftTrigger(ctx, &Empty{})
	return err
}

// StopAcq stops image acquistion mode
func (c *Client) StopAcq(ctx context.Context) error {
	_, err := c.imageAcqClient.StopAcq(ctx, &Empty{})
	return err
}

// StreamReader provides an io.Reader interface for the image returned by
// SnapshotWithData.
//
// Reference: https://encore.dev/blog/git-clone-grpc
type StreamReader struct {
	stream ImageAcq_AcquireClient
	buf    []byte
}

func (sr *StreamReader) Read(p []byte) (int, error) {
	// If we have remaining data from the previous message we received
	// from the stream, simply return that.
	if len(sr.buf) > 0 {
		n := copy(p, sr.buf)
		sr.buf = sr.buf[n:]
		return n, nil
	}

	// No more buffered data, wait for a new message from the stream.
	msg, err := sr.stream.Recv()
	if err != nil {
		return 0, err
	}
	// Read as much data as possible directly to the waiting caller.
	// Anything remaining beyond that gets buffered until the next Read call.
	n := copy(p, msg.Data)
	sr.buf = msg.Data[n:]
	return n, nil
}

// Client to test image streaming
type TestClient struct {
	*imageTestClient
}

func NewTestClient(conn *grpc.ClientConn) *TestClient {
	c := TestClient{}
	c.imageTestClient = &imageTestClient{conn}
	return &c
}

func (c *TestClient) GetImage(ctx context.Context) (*StreamReader, error) {
	stream, err := c.imageTestClient.GetImage(ctx, &Empty{})
	if err != nil {
		return nil, err
	}
	return &StreamReader{stream: stream}, nil
}
