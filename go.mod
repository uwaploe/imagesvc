module bitbucket.org/uwaploe/imagesvc

require (
	github.com/golang/protobuf v1.4.1
	golang.org/x/net v0.0.0-20191009170851-d66e71096ffb // indirect
	golang.org/x/sys v0.0.0-20191009170203-06d7bd2c5f4f // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
)

go 1.13
